import WelcomePage from "./WelcomePage";
import OnBoardingPage from "./Onboarding";
import HomePage from "./HomePage";
import LoginPage from "./LoginPage";

export { WelcomePage, OnBoardingPage, HomePage, LoginPage };
