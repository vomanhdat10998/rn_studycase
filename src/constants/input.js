import { COLORS } from "./colors";

export const INPUT_STYLES = {
  input: {
    label: {
      fontSize: 10,
      fontWeight: 500,
      color: COLORS.gray[400],
      textTransform: "uppercase",
      lineHeight: 12,
      marginBottom: 10,
    },
    inputLogin: {
      fontSize: 16,
      borderBottomWidth: 1,
      borderBottomColor: "#D2D2D2",
      paddingBottom: 4,
      borderRadius: 1,
    },
  },
};
