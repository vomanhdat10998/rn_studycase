import { WelcomePage, OnBoardingPage, HomePage, LoginPage } from "../pages";

import { Button } from "react-native";
import BackButton from "../components/BackButton";

export const ROUTES = [
  {
    name: "Welcome Page",
    page: WelcomePage,
    optionals: {
      headerShown: false,
    },
  },
  {
    name: "Onboarding Page",
    page: OnBoardingPage,
    optionals: {
      headerShown: false,
    },
  },
  {
    name: "Home Page",
    page: HomePage,
    optionals: {},
  },
  {
    name: "Login Page",
    page: LoginPage,
    optionals: {
      headerShadowVisible: false,
      headerTitle: () => <BackButton />,
      headerLeft: () => {
        <Button onPress={() => {}}>Back</Button>;
      },
    },
  },
];
