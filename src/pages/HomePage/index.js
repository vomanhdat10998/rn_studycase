import React, { memo } from "react";
import { Button } from "react-native";
import { Text, View } from "react-native";

function HomePage({ navigation }) {
  return (
    <View>
      <Text>HomePage</Text>

      <Button title="Login" onPress={() => navigation.navigate("Login Page")} />
    </View>
  );
}

export default memo(HomePage);
