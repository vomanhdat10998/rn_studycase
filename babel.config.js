module.exports = function (api) {
  api.cache(true);

  return {
    presets: ["module:metro-react-native-babel-preset"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["."],
          extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
          alias: {
            components: "./src/components",
            pages: "./src/pages",
            routes: "./src/routes",
            utils: "./src/utils",
            constants: "./src/constants",
            assets: "./assets/",
            hooks: "./src/hooks",
          },
        },
      ],
    ],
  };
};
