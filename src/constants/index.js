import { StyleSheet } from "react-native";
import { TEXT_STYLES } from "./text";
import { ROOT_STYLE } from "./root";
import { BUTTON_STYLES } from "./button";
import { INPUT_STYLES } from "./input";

export const styles = StyleSheet.create({
  ...TEXT_STYLES,
  ...ROOT_STYLE,
  ...BUTTON_STYLES,
  ...INPUT_STYLES,
});
