import { ImageBackground, Text, View, SafeAreaView } from "react-native";
import BgWelcome from "../../../assets/images/bg-welcome.png";
import SVGWelcome from "./icon-welcome.svg";
import PrimaryButton from "../../components/PrimaryButton";

function WelcomePage({ navigation }) {
  return (
    <ImageBackground
      resizeMode="cover"
      source={BgWelcome}
      style={{
        flex: 1,
      }}
    >
      <SafeAreaView
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            marginTop: 80,
          }}
        >
          <SVGWelcome width={70} height={70} />

          <Text
            style={{
              maxWidth: 193,
              alignItems: "center",
              marginTop: 25,
              fontSize: 32,
              fontWeight: 800,
              textAlign: "center",
              color: "#ffffff",
            }}
          >
            Welcome to Caroline!
          </Text>

          <Text
            style={{
              maxWidth: 300,
              alignItems: "center",
              marginTop: 18,
              fontSize: 14,
              fontWeight: 500,
              textAlign: "center",
              color: "#ffffff",
              paddingRight: 40,
              paddingLeft: 40,
              lineHeight: 26,
            }}
          >
            Are you ready for the journey full of smiles?
          </Text>
        </View>

        <View
          style={{
            width: 315,
            justifyContent: "center",
            marginBottom: 42,
            alignItems: "center",
          }}
        >
          <PrimaryButton
            text="Get Started"
            positionIcon="right"
            onHandlePress={() => navigation.navigate("Onboarding Page")}
          />
        </View>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default WelcomePage;
