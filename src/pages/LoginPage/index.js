import React, { memo } from "react";
import { SafeAreaView, Text, View } from "react-native";
import Logo from "./login-heart.svg";
import SVGNext from "./icon-next.svg";
import InputField from "../../components/InputField";
import { styles } from "../../constants";
import { TouchableOpacity } from "react-native";

function LoginPage() {
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{
            width: 307,
            justifyContent: "space-between",
            height: "100%",
          }}
        >
          <View>
            <Logo width={100} height={100} />

            <View>
              <Text
                style={{
                  ...styles.headTextPrimary,
                  marginBottom: 10,
                }}
              >
                Create Account
              </Text>

              <Text style={{ ...styles.paraTextPrimary, width: 250 }}>
                Just one more small step, let's start with your email
              </Text>
            </View>

            <View
              style={{
                marginTop: 66,
              }}
            >
              <InputField label="Full name" />

              <InputField label="Email Address" />

              <InputField label="Password" />
            </View>
          </View>

          <View
            style={{
              marginBottom: 42,
            }}
          >
            <TouchableOpacity
              style={{
                height: 70,
                backgroundColor: "#FF4A4A",
                width: "100%",
                justifyContent: "space-between",
                borderRadius: 35,
                flexDirection: "row",
                padding: 26,
              }}
            >
              <Text></Text>

              <Text
                style={{
                  color: "#FFFFFF",
                  fontSize: 16,
                  fontWeight: 600,
                }}
              >
                Create New Account
              </Text>

              <SVGNext width={24} height={24} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default memo(LoginPage);
