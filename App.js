import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import HomePage from "./src/pages/HomePage";
import LoginPage from "./src/pages/LoginPage";
import { Text } from "react-native";
import { Button } from "react-native";
import { useFonts } from "expo-font";
import { useCallback } from "react";

import * as SplashScreen from "expo-splash-screen";
import BackButton from "./src/components/BackButton";
import WelcomePage from "./src/pages/WelcomePage";
import OnBoardingPage from "./src/pages/Onboarding";
import { ROUTES } from "./src/routes";

const Stack = createNativeStackNavigator();

export default function App() {
  const [fontsLoaded] = useFonts({
    SourceSansPro: require("./assets/fonts/SourceSansPro-Light.ttf"),
  });

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }

  return (
    <NavigationContainer onLayout={onLayoutRootView}>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      >
        {ROUTES.map((route, key) => {
          return (
            <Stack.Screen
              key={key}
              name={route.name}
              component={route.page}
              options={route.optionals}
            />
          );
        })}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
