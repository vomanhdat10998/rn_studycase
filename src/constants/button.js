export const BUTTON_STYLES = {
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
  primaryButton: {
    height: 70,
    backgroundColor: "#FF4A4A",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 35,
    flexDirection: "row",
    paddingRight: 26,
    paddingLeft: 26,

    text: {
      color: "#FFFFFF",
      fontSize: 16,
      fontWeight: 600,
      lineHeight: 24,
    },
  },
};
