export const COLORS = {
  yellow: {
    400: "#FFBB24",
  },
  gray: {
    400: "#949494",
  },
  white: {
    400: "#ffffff",
  },
};
