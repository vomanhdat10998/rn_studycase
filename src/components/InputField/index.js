import React, { useState } from "react";
import { Text, TextInput, View } from "react-native";
import { styles } from "../../constants";

function InputField(props) {
  const { label, onChange } = props;
  const [value, setValue] = useState("");

  const handleChangeInput = (e) => {
    onChange && onChange(e);
    setValue(e);
  };

  return (
    <View
      style={{
        marginBottom: 20,
      }}
    >
      <Text style={{ ...styles.input.label }}>{label}</Text>

      <TextInput
        onChangeText={handleChangeInput}
        value={value}
        style={{ ...styles.input.inputLogin }}
      />
    </View>
  );
}

export default InputField;
