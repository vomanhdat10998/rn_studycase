import { COLORS } from "./colors";

export const ROOT_STYLE = {
  container: {
    flex: 1,
    backgroundColor: COLORS.white[400],
    padding: 12,
  },
};
