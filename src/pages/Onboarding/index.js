import { useRef, useState } from "react";
import {
  Dimensions,
  ImageBackground,
  Text,
  TouchableOpacity,
} from "react-native";

// import picture1 from "assets/images/onboarding/picture_1.png";
import picture1 from "../../../assets/images/onboarding/picture_1.png";
import picture2 from "assets/images/onboarding/picture_2.png";
import picture3 from "assets/images/onboarding/picture_3.png";
import { FlatList } from "react-native";
import { View } from "react-native";
import PrimaryButton from "components/PrimaryButton";

import SvgBack from "assets/icons/icon-backlight.svg";
import SvgLocation from "assets/icons/icon-location-radius.svg";

const { height, width } = Dimensions.get("window");

const ONBOARDING_CONSTANTS = [
  {
    name: "step 1",
    index: 0,
    title: "Welcome to Caroline!",
    description: "What do you want to do today?",
    image: picture1,
  },
  {
    name: "step 2",
    index: 1,
    title: "Welcome to Caroline!",
    description: "I can help you find a destination?",
    image: picture2,
  },
  {
    name: "step 3",
    index: 2,
    title: "Welcome to Caroline!",
    description: "Are you ready for the journey full of smiles?",
    image: picture3,
  },
];

const Slide = ({ item }) => {
  return (
    <View
      style={{
        width,
        alignItems: "center",
      }}
    >
      <View
        style={{
          width: 300,
        }}
      >
        <SvgLocation width={70} height={70} />

        <Text
          style={{
            color: "#fff",
            marginTop: 25,
            marginBottom: 18,
            fontSize: 32,
            fontWeight: 800,
          }}
        >
          {item.title}
        </Text>

        <Text
          style={{
            color: "#fff",
            fontSize: 14,
            fontWeight: 600,
            marginBottom: 38,
          }}
        >
          {item.description}
        </Text>
      </View>
    </View>
  );
};

function OnBoardingPage({ navigation }) {
  const [step, setStep] = useState(0);
  const ref = useRef();

  const updateCurrentSlideIndex = (e) => {
    const contentOffsetX = e.nativeEvent.contentOffset.x;

    const currentIndex = Math.round(contentOffsetX / width);

    setStep(currentIndex);
  };

  const handleNextStep = () => {
    if (step === ONBOARDING_CONSTANTS.length - 1) {
      return;
    } else {
      const nextStepIndex = step + 1;

      setStep(nextStepIndex);
      const offset = nextStepIndex * width;

      ref?.current.scrollToOffset({ offset });
    }
  };

  const handlePrevStep = () => {
    if (step === 0) {
      navigation.navigate("Welcome Page");
    } else {
      const prevStepIndex = step - 1;

      setStep(prevStepIndex);
      const offset = prevStepIndex * width;

      ref?.current.scrollToOffset({ offset });
    }
  };

  const footer = () => {
    return (
      <View
        style={{
          flex: 0.5,
          justifyContent: "flex-end",
          marginBottom: 40,
          alignItems: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: 315,
          }}
        >
          <TouchableOpacity
            onPress={handlePrevStep}
            style={{
              width: "30%",
            }}
          >
            <SvgBack
              width={24}
              height={24}
              style={{
                color: "#ffffff",
              }}
            />
          </TouchableOpacity>

          <View
            style={{
              width: "50%",
            }}
          >
            <PrimaryButton
              text="Next"
              positionIcon="right"
              onHandlePress={handleNextStep}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <ImageBackground
      resizeMode="cover"
      source={ONBOARDING_CONSTANTS[step].image}
      style={{
        flex: 1,
        justifyContent: "space-between",
      }}
    >
      <View>
        <FlatList
          ref={ref}
          onMomentumScrollEnd={updateCurrentSlideIndex}
          horizontal
          data={ONBOARDING_CONSTANTS}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            marginTop: 80,
            height: height * 0.3,
          }}
          pagingEnabled
          renderItem={({ item }) => {
            return <Slide item={item} />;
          }}
        />

        {/* Indicator */}
        <View
          style={{
            width,
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 300,
              flexDirection: "row",
              marginTop: 38,
            }}
          >
            {ONBOARDING_CONSTANTS.map((item, index) => {
              return (
                <Text
                  style={[
                    {
                      color: "#fff",
                      marginHorizontal: 4,
                      borderBottomColor: "transparent",
                      borderBottomWidth: 1.5,
                      paddingRight: 5,
                      paddingBottom: 4,
                      fontSize: 12,
                    },
                    step === index && {
                      borderBottomColor: "#FFBB24",
                    },
                  ]}
                  key={index}
                >
                  {index + 1}
                </Text>
              );
            })}
          </View>
        </View>
      </View>

      {footer()}
    </ImageBackground>
  );
}

export default OnBoardingPage;
