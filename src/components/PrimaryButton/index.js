import { Text, TouchableOpacity } from "react-native";

import SVGNext from "../../pages/LoginPage/icon-next.svg";

import { styles } from "../../constants";

function PrimaryButton(props) {
  const { text, positionIcon = "right", customIcon, onHandlePress } = props;

  const handlePress = () => {
    onHandlePress && onHandlePress();
  };

  const renderIcon = () => {
    if (customIcon) {
      return customIcon;
    } else {
      return <SVGNext width={24} height={24} />;
    }
  };

  return (
    <TouchableOpacity
      style={{
        ...styles.primaryButton,
      }}
      onPress={handlePress}
      activeOpacity={0.8}
    >
      {positionIcon === "left" ? (
        <>{renderIcon()}</>
      ) : (
        <Text
          style={{
            width: 24,
          }}
        ></Text>
      )}

      <Text
        style={{
          ...styles.primaryButton.text,
        }}
      >
        {text}
      </Text>

      {positionIcon === "right" ? (
        <>{renderIcon()}</>
      ) : (
        <Text
          style={{
            width: 24,
          }}
        ></Text>
      )}
    </TouchableOpacity>
  );
}

export default PrimaryButton;
