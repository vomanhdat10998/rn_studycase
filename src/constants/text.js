import { COLORS } from "./colors";

export const TEXT_STYLES = {
  headTextPrimary: {
    color: COLORS.yellow[400],
    fontSize: 32,
    fontWeight: 800,
    fontFamily: "SourceSansPro",
  },
  paraTextPrimary: {
    color: COLORS.gray[400],
    fontSize: 14,
    fontFamily: "SourceSansPro",
    fontWeight: 700,
    lineHeight: 26,
  },
};
