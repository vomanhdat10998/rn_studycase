import React from "react";
import { TouchableOpacity } from "react-native";
import SVGBack from "./icon-back.svg";

function BackButton() {
  return (
    <TouchableOpacity>
      <SVGBack width={24} height={24} />
    </TouchableOpacity>
  );
}

export default BackButton;
